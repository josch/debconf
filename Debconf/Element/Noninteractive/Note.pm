#!/usr/bin/perl

=head1 NAME

Debconf::Element::Noninteractive::note - dummy note Element

=cut

package Debconf::Element::Noninteractive::Note;
use warnings;
use strict;
use base qw(Debconf::Element::Noninteractive);

=head1 DESCRIPTION

This is a dummy note element.

=cut

1
